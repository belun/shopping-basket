package exercise.interview.shoppingbasket.io;

import exercise.interview.shoppingbasket.domain.ShoppingBasket;

import java.io.PrintStream;

public class Receipt {
    private final ShoppingBasket shoppingBasket;

    public Receipt(ShoppingBasket shoppingBasket) {
        this.shoppingBasket = shoppingBasket;
    }

    public void printTo(PrintStream printStream) {
        printStream.println("Receipt\n");
        shoppingBasket.printTo(printStream);
    }
}
