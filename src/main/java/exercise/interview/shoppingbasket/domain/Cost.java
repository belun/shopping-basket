package exercise.interview.shoppingbasket.domain;

public class Cost {
    private int centimes;

    public static final Cost ZERO = new Cost();

    public Cost(int centimes) {
        this.centimes = centimes;
    }

    private Cost() {
        this(0);
    }

    public int inCentimes() {
        return centimes;
    }

    public Cost add(int centimes) {
        return new Cost(this.centimes + centimes);
    }

    public Cost add(int centimes, int times) {
        return new Cost(this.centimes + centimes * times);
    }
}
