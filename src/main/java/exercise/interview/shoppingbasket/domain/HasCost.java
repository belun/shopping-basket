package exercise.interview.shoppingbasket.domain;

public interface HasCost {
    Cost addTo(Cost cost, int times);
    Cost addTo(Cost cost);
}
