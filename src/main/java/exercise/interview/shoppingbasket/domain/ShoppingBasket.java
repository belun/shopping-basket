package exercise.interview.shoppingbasket.domain;

import exercise.interview.shoppingbasket.domain.product.Product;

import java.io.PrintStream;

public class ShoppingBasket implements HasCost {
    private final Product[] products;

    public ShoppingBasket(Product... products) {
        this.products = products;
    }

    @Override
    public Cost addTo(Cost cost, int times) {
        Cost totalCost = cost;
        for (int index = 0; index < products.length; index++) {
            totalCost = products[index].addTo(totalCost, times);
        }
        return totalCost;
    }

    @Override
    public Cost addTo(Cost cost) {
        Cost totalCost = cost;
        for (int index = 0; index < products.length; index++) {
            totalCost = products[index].addTo(totalCost);
        }
        return totalCost;
    }

    public int costInCentimes() {
        return addTo(Cost.ZERO).inCentimes();
    }

    public void printTo(PrintStream printStream) {
        for (int index = 0; index < products.length; index++) {
            products[index].printTo(printStream);
        }
        printStream.format("\nTotal : %d", costInCentimes());
    }
}
