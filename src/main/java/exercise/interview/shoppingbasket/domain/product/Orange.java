package exercise.interview.shoppingbasket.domain.product;

public class Orange extends Product {
    public static final Product ONE = new Orange();

    private Orange() {
        super("Orange",30);
    }
}