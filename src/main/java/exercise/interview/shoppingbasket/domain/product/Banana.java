package exercise.interview.shoppingbasket.domain.product;

public class Banana extends Product {
    public static final Product ONE = new Banana();

    private Banana() {
        super("Banana", 15);
    }
}