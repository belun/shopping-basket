package exercise.interview.shoppingbasket.domain.product;

import exercise.interview.shoppingbasket.domain.Cost;
import exercise.interview.shoppingbasket.domain.HasCost;

// TODO : this introduces coupling from business object to a specific presentation layer (java.io)
//  use StringBuilder to output receipt information and let only the Receipt be a presentation object (it can depend on java.io)
import java.io.PrintStream;

public class Product implements HasCost {
    private final int costInCentimes;
    private final String name;

    public Product(String name, int costInCentimes) {
        this.name = name;
        this.costInCentimes = costInCentimes;
    }

    public Product(Product product, int costInCentimes) {
        this.name = product.name;
        this.costInCentimes = costInCentimes;
    }

    public Cost addTo(Cost cost, int times) {
        return cost.add(this.costInCentimes, times);
    }

    public Cost addTo(Cost cost) {
        return cost.add(this.costInCentimes);
    }

    public void printTo(PrintStream printStream) {
        printStream.format("\n%s = %d", name, costInCentimes);
    }
}
