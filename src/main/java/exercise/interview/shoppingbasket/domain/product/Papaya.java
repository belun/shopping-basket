package exercise.interview.shoppingbasket.domain.product;

public class Papaya extends Product {
    public static final Product ONE = new Papaya();

    private Papaya() {
        super("Papaya", 50);
    }
}