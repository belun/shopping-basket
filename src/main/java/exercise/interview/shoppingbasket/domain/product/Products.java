package exercise.interview.shoppingbasket.domain.product;

import exercise.interview.shoppingbasket.domain.Cost;

public class Products {

    public static Product multipleOf(Product product, int times) {
        return new Product(product, product.addTo(Cost.ZERO, times).inCentimes());
    }
}
