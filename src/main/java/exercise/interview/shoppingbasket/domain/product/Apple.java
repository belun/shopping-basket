package exercise.interview.shoppingbasket.domain.product;

public class Apple extends Product {
    public static final Product ONE = new Apple();

    private Apple() {
        super("Apple", 25);
    }
}
