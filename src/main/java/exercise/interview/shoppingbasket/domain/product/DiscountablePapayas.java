package exercise.interview.shoppingbasket.domain.product;

import exercise.interview.shoppingbasket.domain.Cost;

public class DiscountablePapayas extends Product {

    public DiscountablePapayas(int numberOfPapayas) {
        super("Papaya", costOf(numberOfPapayas - freeOutOf(numberOfPapayas)).inCentimes());
    }

    private static int freeOutOf(int numberOfPapayas) {
        return numberOfPapayas / 3;
    }

    private static Cost costOf(int numberOfPapayas) {
        return Papaya.ONE.addTo(Cost.ZERO, numberOfPapayas);
    }
}
