package exercise.interview.shoppingbasket.domain;

import exercise.interview.shoppingbasket.domain.Cost;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class CostTest {
    @Test
    public void shouldBeZeroInitially() {
        assertThat(Cost.ZERO.inCentimes(), is(0));
    }

    @Test
    public void shouldBeFixed() {
        assertThat(new Cost(123).inCentimes(), is(123));
    }

    @Test
    public void shouldAccumulateOneValue() {
        assertThat(new Cost(123).add(234).inCentimes(), is(357));
    }

    @Test
    public void shouldAccumulateMultipleValues() {
        assertThat(new Cost(123).add(234).add(345).inCentimes(), is(702));
    }

    @Test
    public void shouldAccumulateOneValueMultipleTimes() {
        assertThat(new Cost(123).add(234, 2).inCentimes(), is(591));
    }
}
