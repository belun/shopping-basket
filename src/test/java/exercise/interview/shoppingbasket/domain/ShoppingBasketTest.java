package exercise.interview.shoppingbasket.domain;

import exercise.interview.shoppingbasket.domain.ShoppingBasket;
import exercise.interview.shoppingbasket.domain.product.*;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ShoppingBasketTest {

    @Test
    public void shouldCharge120CentimesForOneOfEach() {
        assertThat(new ShoppingBasket(Apple.ONE, Orange.ONE, Banana.ONE, Papaya.ONE).costInCentimes(), is(120));
    }

    @Test
    public void shouldCharge310CentimesFor3OfEach() {
        assertThat(new ShoppingBasket(
                Products.multipleOf(Apple.ONE, 3),
                Products.multipleOf(Orange.ONE, 3),
                Products.multipleOf(Banana.ONE, 3),
                new DiscountablePapayas(3))
                .costInCentimes(), is(310));
    }
}
