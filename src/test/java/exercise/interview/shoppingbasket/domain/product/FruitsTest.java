package exercise.interview.shoppingbasket.domain.product;

import exercise.interview.shoppingbasket.domain.Cost;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class FruitsTest {
    @Test
    public void shouldCharge25CentimesPerApple() {
        assertThat(Apple.ONE.addTo(Cost.ZERO).inCentimes(), is(25));
    }

    @Test
    public void shouldCharge305CentimesPerOrange() {
        assertThat(Orange.ONE.addTo(Cost.ZERO).inCentimes(), is(30));
    }

    @Test
    public void shouldCharge15CentimesPerBanana() {
        assertThat(Banana.ONE.addTo(Cost.ZERO).inCentimes(), is(15));
    }

    @Test
    public void shouldCharge50CentimesPerPapaya() {
        assertThat(Papaya.ONE.addTo(Cost.ZERO).inCentimes(), is(50));
    }
}
