package exercise.interview.shoppingbasket.domain.product;

import exercise.interview.shoppingbasket.domain.Cost;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ProductTest {
    @Test
    public void shouldUpdateAdd20ToCost() {
        assertThat(new Product("Product", 20).addTo(new Cost(10)).inCentimes(), is(30));
    }
}
