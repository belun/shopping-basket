package exercise.interview.shoppingbasket.domain.product;

import exercise.interview.shoppingbasket.domain.Cost;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DiscountablePapayasTest {
    @Test
    public void shouldCharge100CentimesPer3Papayas() {
        assertThat(new DiscountablePapayas(3).addTo(Cost.ZERO).inCentimes(), is(100));
    }

    @Test
    public void shouldCharge100CentimesPer2Papayas() {
        assertThat(new DiscountablePapayas(2).addTo(Cost.ZERO).inCentimes(), is(100));
    }

    @Test
    public void shouldCharge150CentimesPer4Papayas() {
        assertThat(new DiscountablePapayas(4).addTo(Cost.ZERO).inCentimes(), is(150));
    }

    @Test
    public void shouldCharge50CentimesPer1Papaya() {
        assertThat(new DiscountablePapayas(1).addTo(Cost.ZERO).inCentimes(), is(50));
    }

    @Test
    public void shouldCharge200CentimesPer6Papayas() {
        assertThat(new DiscountablePapayas(6).addTo(Cost.ZERO).inCentimes(), is(200));
    }
}
