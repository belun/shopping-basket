package exercise.interview.shoppingbasket.io;

import exercise.interview.shoppingbasket.domain.ShoppingBasket;
import exercise.interview.shoppingbasket.io.Receipt;
import exercise.interview.shoppingbasket.domain.product.Apple;
import exercise.interview.shoppingbasket.domain.product.Banana;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class ReceiptTest {

    @Test
    public void shouldShowTotalZeroOnReceipt() {
        Receipt receipt = new Receipt(new ShoppingBasket());

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        receipt.printTo(new PrintStream(byteArrayOutputStream));

        receipt.printTo(System.out);
        assertThat(byteArrayOutputStream.toString(), containsString("Total : 0"));
    }

    @Test
    public void shouldShowOneBananaOnReceipt() {
        Receipt receipt = new Receipt(new ShoppingBasket(Banana.ONE));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        receipt.printTo(new PrintStream(byteArrayOutputStream));

        receipt.printTo(System.out);
        assertThat(byteArrayOutputStream.toString(), containsString("Banana = 15"));
    }

    @Test
    public void shouldShowOneBananaAndOneAppleAndTotalOnReceipt() {
        Receipt receipt = new Receipt(new ShoppingBasket(Banana.ONE, Apple.ONE));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        receipt.printTo(new PrintStream(byteArrayOutputStream));

        receipt.printTo(System.out);
        assertThat(byteArrayOutputStream.toString(), containsString("Banana = 15"));
        assertThat(byteArrayOutputStream.toString(), containsString("Apple = 25"));
        assertThat(byteArrayOutputStream.toString(), containsString("Total : 40"));
    }

    @Test
    public void shouldShowThreeBananaOnReceipt() {
        // TODO : to make this test pass, a Product must have Quantity object inside
        //  the construct Product.multipleOf() is no longer enough as it cannot participate in the printing the receipt

        Receipt receipt = new Receipt(new ShoppingBasket(Banana.ONE));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        receipt.printTo(new PrintStream(byteArrayOutputStream));

        receipt.printTo(System.out);
        assertThat(byteArrayOutputStream.toString(), containsString("Banana(s) : 15 x 3 = 45"));
    }
}
